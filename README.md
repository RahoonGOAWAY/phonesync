# Things it should do:

## copy over
- [ ] signal backup
- [ ] pictures

### other app configs?? idk how/where exactly, but probably through a seedvault file?

## sync
- [ ] documents
- [ ] todo?(tasks or something else)
- [x] obsidian
- [ ] joplin?
- [ ] newpipe
- [ ] contacts
- [x] backgorunds
- [ ] music

# Overall project todos:
- [ ] automate making backups through individual apps because right now each one has like 5 menus to go through manually
- [ ] config file
- [ ] first time wizard?
- [ ] modularize because most of the rsync commands are the same
- [ ] auto-mount and get phone path
	- [ ] autorun when phone plugged in? Maybe send notification asking first
- [ ] AUR??
